//
//  ViewController.swift
//  TEDWheel
//
//  Created by Steve Edwards on 14/02/2015.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var wheelView: TEDWheelView!
    @IBOutlet weak var valueOutput: UILabel!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Register to be notified when the value stored in the wheel is updated
        let options : NSKeyValueObservingOptions = .New
        wheelView.addObserver(self, forKeyPath: "value", options: options, context: nil)
    }
    
    override func observeValueForKeyPath(keyPath: String, ofObject object: AnyObject, change: [NSObject : AnyObject], context: UnsafeMutablePointer<Void>) {
        
        switch keyPath {
        case "value":
            
            // The value property of the wheel has updated, do something with it
            valueOutput.text = "\(wheelView.value)"
        
        default:
            break
        }
    }

}

