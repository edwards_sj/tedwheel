//
//  TEDWheelView.swift
//
//  Created by Steve Edwards on 12/02/2015.
//

import UIKit

@IBDesignable
public class TEDWheelView: UIView {
    
    dynamic public var value: Int = 0 {
        didSet {
            value = min(max(value, minValue), maxValue)
            println("TEDWheel value: \(value)")
        }
    }
    
    @IBInspectable
    public var maxValue: Int = Int.max {
        didSet {
            value = min(value, maxValue)
        }
    }
    
    @IBInspectable
    public var minValue: Int = Int.min {
        didSet {
            value = max(value, minValue)
        }
    }
    
    @IBInspectable
    public var wheelInset: CGFloat = 15.0 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    private var startAngle: CGFloat = 0.0
    private var startTransform: CGAffineTransform?
    
    private func setup() {
        // Using a pan gesture so we can get direction and speed
        self.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: "pan:"))
        self.opaque = false
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required public init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    public func pan(gesture: UIPanGestureRecognizer) {
        
        // Get the angle of the touchpoint from the center point
        let touchPoint = gesture.locationInView(superview)
        let dx = touchPoint.x - center.x;
        let dy = touchPoint.y - center.y;
        let angle = atan2(dy, dx)
        
        switch gesture.state {
        case .Began:
            
            // Pan gesture begins, store the angle of the touch from the center
            // point and the initial transform attached to theview
            startAngle = angle
            startTransform = transform
            
        case .Changed:
            
            // Position has moved, calculate the angle difference between this
            // and the last point and apply it to the view transform to rotate it
            let angleDifference = startAngle - angle
            transform = CGAffineTransformRotate(startTransform!, -angleDifference)
            
            // Get the velocity of the move and use this to set an increment value
            // so that the value will change faster if the gesture is performed quickly
            let velocity = gesture.velocityInView(superview)
            var step = Int(max(abs(velocity.x), abs(velocity.y)) / 100) + 1

            // Check to see if we are moving anticlockwise, if we are then the
            // value will go down
            if touchPoint.x >= center.x {
                if touchPoint.y <= center.y {
                    if min(velocity.x,velocity.y) < 0 {
                        step = -step
                    }
                }
                else if velocity.x > 0 || velocity.y < 0 {
                    step = -step
                }
            }
            else {
                if touchPoint.y >= center.y {
                    if max(velocity.x, velocity.y) > 0 {
                        step = -step
                    }
                }
                else if velocity.x < 0 || velocity.y > 0 {
                    step = -step
                }
                
            }
            
            // Step will be positive if we are travelling colckwise, otherwise
            // it will be negative
            value += step
            
            // Reset the gesture translation so we are always getting incrememtal
            // translations
            gesture.setTranslation(CGPointZero, inView: superview)
            
        default: break
        }
    }
    
    override public func drawRect(rect: CGRect) {
        
        let inset = CGRectInset(rect, wheelInset, wheelInset)
        
        // Draw anything you like in here

        UIColor.blackColor().set()
        UIColor.whiteColor().setFill()
        
        var bezierPath = UIBezierPath(ovalInRect: CGRectInset(inset, 10, 10))
        bezierPath.stroke()

        bezierPath = UIBezierPath(ovalInRect: CGRectMake(CGRectGetMidX(inset) - 10, CGRectGetMinY(inset), 20, 20))
        bezierPath.stroke()
        bezierPath.fill()

        bezierPath = UIBezierPath(ovalInRect: CGRectMake(CGRectGetMidX(inset) - 10, CGRectGetMaxY(inset) - 20, 20, 20))
        bezierPath.stroke()
        bezierPath.fill()

    }

}
