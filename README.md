TEDWheel
========

![TEDWheel-Demo.png](https://bitbucket.org/repo/Rj8e46/images/2574009783-TEDWheel-Demo.png)

A UIView subclass that acts like a rotary control.

The control will spin forever and is not limited to a single rotation.

A value integer will be incremented when the user rotates the control clockwise and decremented if anticlockwise.

Integer value can be restricted to a range.

The value will change in variable steps based on the speed of the gesture.

To change the look of the control, just add your code into the drawRect method.

Written in Swift.

## Usage

```Swift
var wheel = TEDWheelView(frame: CGRectMake(100, 100, 200, 200))
wheel.maxValue = 100
wheel.minValue = 0
wheel.value = 50
self.view.addSubview(wheel)
```

## License

TEDWheel is available under the MIT license. See the LICENSE file for more info.